# About

Program for the blocked pattern matching problem. If you feel it is helpful, please cite

Fei Deng, Lusheng Wang and Xiaowen Liu. "An efficient algorithm for the blocked pattern matching problem." Bioinformatics (2014): btu678.

