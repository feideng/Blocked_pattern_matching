import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;


public class Test {
	
	public void generateACommand(int fixedPatternLength,double searchError, double resultError, int confidence, PrintWriter writer) throws IOException{
		int minPatternLength = fixedPatternLength;
		writer.print("java DoSearch2 test.fasta deng_13135 "+minPatternLength+" "+fixedPatternLength+" ");
		writer.print(searchError+" "+resultError+" "+confidence+" ");
		writer.print("result/output-3-18"+"-p"+fixedPatternLength+"-c"+confidence+"-s"+searchError+"-r"+resultError+" >> ");
		writer.println("result/run-3-18"+"-p"+fixedPatternLength+"-c"+confidence+"-s"+searchError+"-r"+resultError+".log");
		writer.print("java MSGFComparator MSGF-1-29-Q.tsv ");
		writer.print("result/output-3-18"+"-p"+fixedPatternLength+"-c"+confidence+"-s"+searchError+"-r"+resultError+" >> ");
		writer.println("result/run-3-18"+"-p"+fixedPatternLength+"-c"+confidence+"-s"+searchError+"-r"+resultError+".log");
	}
	
	public void generateCommand(String filename) throws IOException{
		PrintWriter writer = new PrintWriter(filename);
		int confidence = 50;
		for (int i=4; i<=10; i++){
			generateACommand(i, 0, 2.0, confidence, writer );
			generateACommand(i, 0.05, 2.0, confidence, writer);
		}
		
		writer.close();
		
	}
	
	public void generateCommand2(String filename) throws IOException{
		PrintWriter writer = new PrintWriter(filename);
		int confidence = 50;
		double searchError = 0.05;
		double resultError = 2.0;
		for (int i=4; i<=8; i++){
			writer.print("java MSGFComparator MSGF-1-29-Q.tsv ");
			writer.print("result/output-3-1"+"-p"+i+"-c"+confidence+"-s"+searchError+"-r"+resultError+" >> ");
			writer.println("result/candidate-3-1"+"-p"+i+"-c"+confidence+"-s"+searchError+"-r"+resultError+".log");
			
		}
	}
	
	public void t(String peptideFile) throws IOException{
		MassTable mt = new MassTable();
		Peptide.PATTERN_LENGTH = 3;
		PeptideFileHandler pf = new PeptideFileHandler(mt);
		HashSet[] tagSet = pf.extractPatterns(peptideFile);
		pf.getTagSetDetails(tagSet);
		
		//pf.getPartialData(peptideFile, "deng_fei_data_top1", 1);
	
		System.out.println("Hello world");
		
	}
	
	public static void main(String[] args) throws IOException{
		/*MassTable mt = new MassTable();
		mt.generateMassTable();
		DatabaseFileHandler df = new DatabaseFileHandler();
		ProteinDatabase pd = df.loadDatabase("test.fasta");
		SuffixTree st = new SuffixTree(pd.getSequence(), pd);
		PatternSearch ps = new PatternSearch(st, mt);
		
		Pattern pattern3 = new Pattern();
		pattern3.add(11308);
		pattern3.add(11308);
		pattern3.add(7104);
		System.out.println(ps.searchPattern(pattern3));
		
		Pattern pattern = new Pattern();
		pattern.add(7104);
		pattern.add(7104);
		
		ArrayList<SuffixPosition> leafSet = ps.searchPattern(pattern);
		System.out.println(leafSet);
		
		Pattern pattern2 = new Pattern();
		pattern2.add(7104);
		pattern2.add(7104);
		pattern2.add(7104);
		System.out.println(ps.searchPattern(pattern2));*/	
		Test tt = new Test();
		tt.generateCommand("run13135-c50.sh");
		//tt.t("deng_fei_data");
		
		
	}

}

class aaa{
	private boolean visited;
	private int num;
	public aaa(boolean hello, int num){
		visited = hello;
		this.num = num;
	}
	public void set(){
		visited = false;
	}
	public String toString(){
		return visited+" "+num;
	}
}
