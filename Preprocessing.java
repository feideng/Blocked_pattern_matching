import java.util.ArrayList;
import java.util.Vector;


public class Preprocessing {
	private SuffixTree suffixTree;
	private ProteinDatabase proteinDatabase;
	private Vector<Vector<Integer>> peptideTable;
	private DoSearch2 doSearch;
	
	public Preprocessing(SuffixTree st, ProteinDatabase pd, DoSearch2 doSearch, Vector<Vector<Integer>> peptideTable){
		suffixTree = st;
		proteinDatabase = pd;
		this.peptideTable = peptideTable;
		this.doSearch = doSearch;
	}
	
	public int computeLeafPeptideMass(LeafEdge edge){
		SuffixPosition sp = edge.getSuffixPosition();
		sp.compPeptidePos2(peptideTable, proteinDatabase);
		String pepString = sp.getPeptideSequence(proteinDatabase);
		edge.setPeptideSequence(pepString);
		int mass = doSearch.computePeptideMass(pepString);
		edge.setPeptideMass(mass);
		sp = null;
		pepString = null;
		
		return mass;
	}
	
	public void computeMassRangeForNodes(Node node){
		for (int i=0; i<21; i++){
			Edge edge = node.getEdge(i);
			if (edge == null) continue;
			Node childNode = edge.getEndNode();
			if (childNode == null)
				computeLeafPeptideMass((LeafEdge)edge);
			else
				computeMassRangeForNodes(childNode);
		}
		
		int min = Integer.MAX_VALUE, max = -1;
		for (int i=0; i<21; i++){
			Edge edge = node.getEdge(i);
			if (edge == null) continue;
			Node childNode = edge.getEndNode();
			if (childNode == null){
				int mass = ((LeafEdge) edge).getPeptideMass();
				if (min > mass)
					min = mass;
				if (max < mass)
					max = mass;
			}
			else{
				if (min > childNode.getMinPeptideMass())
					min = childNode.getMinPeptideMass();
				if (max < childNode.getMaxPeptideMass())
					max = childNode.getMaxPeptideMass();
			}
		}
		
		node.setMinPeptideMass(min-doSearch.getScaledMassTolerance());
		node.setMaxPeptideMass(max+doSearch.getScaledMassTolerance());
	}
	
	/*
	public void computeMassRangeForNodes()
	{
		
		ArrayList<Node> list1 = new ArrayList<Node>();
		ArrayList<Node> list2 = new ArrayList<Node>();
		list1.add(suffixTree.getRoot());
		while (list1.size() > 0){
			for (int i=0; i<list1.size(); i++){
				Node node = list1.get(i);
				boolean done = true;
				for (int j=0; j<21; j++){
					Edge edge = node.getEdge(i);
					Node endNode = edge.getEndNode();
					if (endNode != null){
						list2.add(endNode);
						done = false;
					}
					else{
						int mass = computeLeafPeptideMass((LeafEdge) edge);
						updatePeptideMass(node, mass);
					}
				}
				if (done)
					updatePeptideMass(node);
			}
			
			list1 = list2;
			list2.clear();
		}
	}
	
	public void updatePeptideMass(Node node, int mass){
		if (node.getMinPeptideMass() > mass)
			node.setMinPeptideMass(mass);
		if (node.getMaxPeptideMass() < mass)
			node.setMaxPeptideMass(mass);
	}
	
	public void updatePeptideMass(Node node){
		while (node != null){
			Node parent = node.
			
		}
	}*/
}
